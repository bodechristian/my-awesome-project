from ..math import add

def test_spec():
    assert add(5, 4) == 9
    assert add(-2, 7) == 5

def test_neutral():
    assert add(4, 0) == 4
    assert add(0, 4) == 4

def test_commut():
    assert add(5, 3) == add(3, 5)
    assert add(-1, 4) == add(4, -1)

def test_assoc():
    assert add(3, add(3, 2)) == add(add(3, 3), 2)
    assert add(5, add(-2, 1)) == add(add(5, -2), 1)

