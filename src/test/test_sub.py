from ..math import sub

def test_spec():
    assert sub(4, 2) == 2
    assert sub(6, -2) == 8

def test_neutral():
    assert sub(4, 0) == 4
    assert sub(0, 4) == -4

def test_nocommut():
    assert sub(4, 5) != sub(5, 4)
    assert sub(3, -2) != sub(-2, 3)

